<?php

class Pager {

    private $CurrentPage, $PagesNumbers, $PerPage, $Number, $Get, $page, $Lanugages,
    $Stlyes, $core;

    public function __construct($Engine) {
        if (is_object($Engine)) {
            $this->core = &$Engine;
        }else{
            global $_;
            $this->core = &$Engine;
        }
        $this->PerPage = 3;
        $this->Get = 'p';
        if (!preg_match('/\?/', $_SERVER['PHP_SELF'])) {
            $this->page = $_SERVER['PHP_SELF'] . '?';
        } else {
            $this->page = $_SERVER['PHP_SELF'] . '&';
        }

        $this->Lanugages = array('next' => 'التالي', 'prev' => 'السابق', 'page' =>
            'صفحة', 'from' => 'من');

        $this->Styles = array('current' => 'current', 'disabled' => 'disabled',
            'paginator-msg' => 'pagmsg', 'paginator' => 'pagination');
    }

    /**
     * the number of result
     * @access public
     * @return the number of result
     */

    public function GetNumber() {
        return (int) $this->Number;
    }

    /**
     * set per page of pagination
     * @param per page
     * @access public
     */

    public function SetPerPage($PerPage) {
        $this->PerPage = intval(abs($PerPage));
    }

    /*
     * get per page of pagination
     * @access : public
     * @return : per page
     */

    public function GetPerPage() {
        return $this->PerPage;
    }

    /*
     * Set the $_GET of pagination
     * @param : get
     * @access : public
     */

    public function SetGetPage($get) {
        $this->Get = $get;
    }

    /*
     * get the $_GET of pagination
     * @access : public
     * @return : get of page
     */

    public function GetGetPage() {
        return $this->Get;
    }

    /**
     * set the url of page
     * @param url
     * @access public
     */

    public function SetPageUrl($url) {
        if (!preg_match('/\?/', $url)) {
            $url = $url . '?';
        } else {
            $url = $url . '&';
        }
        $this->page = $url;
    }

    /*
     * get the url of page
     * @access : public
     * @return : the url of page
     */

    public function GetPageUrl() {
        return $this->page;
    }

    public function ShowPagesList() {
        if ($this->Number != 0) {
            $select = "\n" . '
                <span class="' . $this->Stlyes['paginator-msg'] . '">
                    ' . $this->Lanugages['page'] . ' ' . $this->CurrentPage .
                    ' ' . $this->Lanugages['from'] . ' ' . $this->PagesNumbers . '
                </span>
                <select onchange="window.location=this.options[this.selectedIndex].value;">';
            for ($i = 1; $i <= $this->PagesNumbers; $i++) {
                if ($i == $this->CurrentPage) {
                    $select .= '<option value="' . $this->page . $this->Get . '=' . $i .
                            '" selected="selected">' . $i . '</option>' . "\n";
                } else {
                    $select .= '<option value="' . $this->page . $this->Get . '=' . $i . '">' . $i .
                            '</option>' . "\n";
                }
            }
            $select .= "</select>";
            return $select;
        }
    }

    /*
     * Create Pagination
     * @param : Query
     * @access : public
     * @return : array with result of Query from database
     */

    public function CreatePaginator($Query) {
        $this->Number = $this->CountPages($Query); // count pages
        $this->PagesNumbers = ceil($this->Number / (float) $this->PerPage); // get page numbers
        // checking get page
        $getPage = isset($_GET[$this->Get]) ? $_GET[$this->Get] : null;
        if ($getPage > 0 && $getPage <= $this->PagesNumbers && is_numeric($getPage)) {
            $this->CurrentPage = intval(abs($getPage)); // set the current page
        } else {
            $this->CurrentPage = 1; // set the current page as the first
        }
        // add limit to the query
        try {

            $Query = $this->core->db->query($Query . " LIMIT " . (($this->CurrentPage - 1) * $this->
                    PerPage) . " , " . $this->PerPage);
            // start the while and set the result in athor array
            $r = array();
            while ($rows = $this->core->db->fetchObject($Query)) {
                array_push($r, $rows);
            }
            mysql_free_result($Query); // free the result
            unset($rows);
        } catch (exception $e) {
            trigger_error($e->getMessage(), E_USER_WARNING);
        }
        return array_values($r);
    }

    /*
     * Count The Page Namber
     * @param: Query
     * @return : Number of Pages
     * @access : priviate
     */

    private function CountPages($Query) {
        
        try {
            $count = $this->core->db->query($Query); // query
            $Number = $this->core->db->resultCount($count); // num
            mysql_free_result($count);
        } catch (exception $e) {
            trigger_error($e->getMessage(), E_USER_ERROR);
        }
        return $Number; // return the number results
    }

    /*
     * Get The Pagination
     * @return : The Pagination
     * @access : public
     */

    public function GetPagination() {
        if ($this->Number > $this->PerPage) {
            $next = $this->CurrentPage + 1; // get next page number
            $prev = $this->CurrentPage - 1; // get prev page number
            $pagination = "\n" . '<div class="' . $this->Styles['paginator'] . '">' . "\n"; // start the pagination
            // get prev
            if ($this->CurrentPage > 1 && $this->CurrentPage <= $this->PagesNumbers) {
                $pagination .= '<span><a href="' . $this->page . $this->Get . '=1">الأولى</a></span> ';
                $pagination .= '<span><a href="' . $this->page . $this->Get . '=' . $prev . '">' .
                        $this->Lanugages['prev'] . '</a></span>' . "\n";
            }
            // get the pagination numbers
            for ($i = 1; $i <= $this->PagesNumbers; $i++) {
                if ($i == $this->CurrentPage) {
                    $pagination .= '<span class="' . $this->Styles['current'] . '">' . $i .
                            '</span>' . "\n";
                } else {
                    $pagination .= '<a href="' . $this->page . $this->Get . '=' . $i . '">' . $i .
                            '</a>' . "\n";
                }
            }
            // get the next page
            if ($this->CurrentPage < $this->PagesNumbers) {
                $pagination .= '<span><a href="' . $this->page . $this->Get . '=' . $next . '">' .
                        $this->Lanugages['next'] . '</a></span>' . "\n";
                $last = $this->GetNumber();
                $pagination .= '<span><a href="' . $this->page . $this->Get . '=' . $last . '">الأخيرة</a></span>';
            }
            $pagination .= '</div>' . "\n";
            return wordwrap($pagination);
        }
    }

    #end of class
}

?>