<?php 

class Info {

	public $_data = array();
	var $_error = array();

	static function get($var_name) {
		global $_;
		$_->db->query("SELECT value FROM {$_->db->_prefix}infos WHERE var_name='$var_name'");
		$r = $_->db->fetchRow();
		return $r['value'];
	}

	function get_info($var_name) {
		return self::get($var_name);
	}

	function get_data($index) {
		return $this->_data[$index];
	}

	function set_data($index,$value) {
		return $this->_data[$index] = $value;
	}

	function where() {
		extract($_GET);
		if(isset($id) && !empty($id)){
			return 'single';
		}elseif(isset($page_id) && !empty($page_id)){
			return 'page';
		}elseif(isset($cat_id) && !empty($cat_id)){
			return 'category';
		}elseif(isset($author) && !empty($author)){
			return 'author';
		}elseif(isset($s) && !empty($s)){
			return 'search';
		}elseif(!empty($_GET) && !isset($s) && !isset($p)){
			return '404';
		}else{
			return 'index';
		}
	}

	function total($type) {
		global $_;
		switch($type) {
			case "posts":
				$_->db->query("SELECT * FROM posts WHERE post_type = 'post'");
				$count = $_->db->resultCount();
				return $count;
				break;
			case "pages":
				$_->db->query("SELECT * FROM posts WHERE post_type = 'page'");
				$count = $_->db->resultCount();
				return $count;
				break;
			case "comments":
				$_->db->query("SELECT * FROM posts WHERE post_type = 'comment'");
				$count = $_->db->resultCount();
				return $count;
				break;
			case "wait_comments":
				$_->db->query("SELECT * FROM posts WHERE post_type = 'comment' and post_stat = 'wait'");
				$count = $_->db->resultCount();
				return $count;
				break;
			case "users":
				$_->db->query("SELECT * FROM users");
				$count = $_->db->resultCount();
				return $count;
				break;
			case "media":
				return 'Under Programming';
				break;
		}
	}

	public function update($args)
	{
		global $_;
		if(is_array($args))
		{
			foreach($args as $key => $val)
			{
				$key = xss_safe($key);
				$val = xss_safe($val);
				$sql = $_->db->query("UPDATE infos SET value = '".$val."' WHERE var_name = '".$key."'");
				if(!$sql)
					$this->error[] = 'Can\'t update the database!';
			}
		}	
	}

	public function has_error()
	{
		if(is_array($this->_error) && !empty($this->_error))
		{
			return true;
		}
	}

	// others infos for others systems
	public function users($id)
	{
		global $_;
		$_->db->query("SELECT * FROM users WHERE user_id IN ('$id') LIMIT 1");
		$r = $_->db->fetchRow();
		return $r;
	}

}