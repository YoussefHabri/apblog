<?php

class Version {
	var $sysName = "APBlog";
	var $sysVer  = "1.0";
	var $sysUrl  = "https://apblog.arabstep.com";
	var $sysStat = "beta-1";
	var $sysCodeName = "Bee Bazaar";

	public function title(){
		return $this->sysName.' '.$this->sysVer.' '.$this->sysStat.'('.$this->sysCodeName.')';
	}

	public function miniTitle(){
		return $this->sysName.' '.$this->sysVer;
	}

	function sys_info()
	{
		$partners = array(
			'arabstep' => array('url'=>'http://www.arabstep.com','name'=>'Arabstep'),
			);

		$sys = array(
			'name' => $this->sysName,
			'version' => $this->sysVer,
			'programmer' => '<a href="'.$this->sysUrl.'/developers">Youssef Habri</a>',
			'website' => '<a href="'.$this->sysUrl.'">'.$this->sysName.' Support</a>',
			'admin_des' => 'Traidnt UP v3.0',
			'partners' => $partners,
			);
		return $sys;
	}

}