<?php

class Plugin {

    private $db; // database pdo :)
    private $core;
    private $file_path; // current plugin file
    public $plugins_folder; // plugins folder
    public $plugins_table = "plugins"; // plugin table folder
    public $plugins_settings_table = "plugins_settings";
    public $database_prefix = ""; // database tables prefix
    private $xml_mime_type = array('text/xml', 'application/xml');
    public $xml_encoding = "utf-8";

    public function __construct($core) {
        if (is_object($core)) {
            $this->db = $core->db;
        }
    }

    public function setXmlEncoding($encoding) {
        $this->xml_encoding = $encoding;
    }

    public function getXmlEncoding() {
        return $this->xml_encoding;
    }

    public function setDatabasePrefix($prefix) {
        $this->database_prefix = $prefix;
    }

    public function getDatabasePrefix($prefix) {
        return $this->database_prefix;
    }

    /*
     * set current plugin file
     * @param : String plugin file
     * @access : public
     */

    public function setFilePath($path) {
        $this->file_path = $path;
    }

    /*
     * get current plugin file name
     * @return : String file name
     * @access : public
     */

    public function getFilePath() {
        return $this->file_path;
    }

    /*
     * set the plugins folder name
     * @param : String folder name
     * @access : public
     */

    public function setPluginsFolder($folder) {
        $this->plugins_folder = $folder;
    }

    /*
     * get the plugins folder name
     * @return : String folder name
     * @access : public
     */

    public function getPluginsFolder() {
        return $this->plugins_folder;
    }

    /*
     * get the extension of file
     * @param : String file name
     * @return : String file extension
     * @access : private
     */

    private function get_extension($file) {
        if (function_exists("pathinfo")) {
            $extension = pathinfo($file, PATHINFO_EXTENSION);
        } else {
            $extension = end(explode(".", $file));
        }
        return trim(strtolower($extension));
    }

    /*
     * get last plugin id
     * @return : INT plugn id
     * @access : private
     */

    private function get_last_plugin_id() {
        $res_last_id = $this->db->query("SELECT id FROM " . $this->database_prefix . $this->plugins_table . " ORDER BY id DESC LIMIT 1");
        $data = $this->db->fetch_object($res_last_id);
        return $data->id;
    }

    public function import_plugin($file) {
        try {
            if (file_exists($file) && $this->get_extension($file) == "xml") {
                $xml = simplexml_load_file($file);
                $plugin_title = strip_tags($xml->title);
                $plugin_version = strip_tags($xml->version);
                $plugin_name = strip_tags($xml->name);
                $plugin_author = strip_tags($xml->author);
                $plugin_description = strip_tags($xml->description);
                if (is_array($xml->place) or is_object($xml->place)) {
                    $places = array();
                    foreach ($xml->place as $p => $place) {
                        $pl = (array) $place;
                        $places[] = $pl[0];
                    }
                    $plugin_place = serialize($places);
                    unset($places, $pl);
                } else {
                    $plugin_place = $xml->place;
                }
                $plugin_status = intval(abs($xml->status));
                $plugin_code = trim(addslashes($xml->code));
                $plugin_install = trim(addslashes($xml->install));
                $plugin_settings = (array) $xml->settings; // convert std class to an array :)

                $insert_plg_info_sql = "INSERT INTO " . $this->database_prefix . $this->plugins_table . "
                                    (author,name,version,description,status,place,code,titre,plugin_file)
                                    VALUES
                                    ('$plugin_author','$plugin_name','$plugin_version','$plugin_description','$plugin_status','$plugin_place','$plugin_code','$plugin_title','$file')";
                $insert_plugin_information = $this->db->query($insert_plg_info_sql);
                if (!$insert_plugin_information) {
                    throw new Exception("Couldnt Install plugin Error :!!!");
                } else {
                    if (!empty($plugin_install)) {
                        $install_plugin = $this->db->query($plugin_install);
                    }
                }
                $plugin_id = $this->get_last_plugin_id();
                $Query = "INSERT INTO " . $this->database_prefix . $this->plugins_settings_table . " 
                                (`name`,`text`,`type`,`valeur`,`plugin_id`) VALUES ";
                $count_settings = count($plugin_settings);
                $i = 0;
                foreach ($plugin_settings as $keys => $values) {
                    $i++;
                    $setting_name = $keys;
                    $setting_text = $values->text;
                    $setting_valeur = $values->valeur;

                    if (isset($values->type['name'])) {
                        $name = (array) $values->type['name'];
                    } else {
                        $name = $values->type;
                    }
                    $setting_type = array();
                    $setting_type['type'] = $name[0];
                    if (isset($values->type->option)) {
                        $count_options[$i] = count($values->type->option);
                        $x = 0;
                        for ($x; $x < $count_options[$i]; $x++) {
                            $val_array[$x] = (array) $values->type->option[$x]["value"];
                            $options[$x]['value'] = $val_array[$x][0];
                            unset($values->type->option[$x]['value']);
                            $content_array[$x] = (array) $values->type->option[$x];
                            $options[$x]['content'] = $content_array[$x][0];
                            unset($val_array[$x], $content_array[$x]);
                        }
                        $setting_type['options'] = $options;
                        unset($options);
                    }

                    $setting_type = serialize($setting_type);
                    $Query .= " ('" . $setting_name . "',
                                 '" . $setting_text . "',
                                 '" . $setting_type . "',
                                 '" . $setting_valeur . "',
                                 '" . $plugin_id . "'
                                ) ";
                    if ($count_settings != $i) {
                        $Query .= " , ";
                    }
                }
                $exec_setting_plugin = $this->db->query($Query);
                if (!$exec_setting_plugin) {
                    throw new Exception("Error in install plugin setting");
                }
                unset($Query, $xml);
            } else {
                throw new Exception("Is not a valid plugin file");
            }
        } catch (Exception $e) {
            trigger_error($e->getMessage(), E_USER_ERROR);
        }
    }

    private function cData($content) {
        return "<![CDATA[ $content ]]>";
    }

    /*
     * check if text is serialized
     * @param : String data to check
     * @return : Boolean
     * @access : public
     */

    public function is_serialized($data) {
        return (@unserialize($data) !== false);
    }

    /*
     * get the plugin file name by id
     * @param : integer id of plugin
     * @return : plugin file name
     * @access : private
     */

    private function get_plugin_file($id) {
        if (!is_int($id)) {
            return false;
        }
        $id = intval(abs($id));
        $res_plg_file = $this->db->query("SELECT plugin_file FROM " . $this->database_prefix . $this->plugins_table . " WHERE id='$id'");
        $data = $this->db->fetch_object($res_plg_file);
        return $data->plugin_file;
    }

    public function export_plugin($id) {
        if (!is_int($id)) {
            return false;
        }
        ob_start();
        $res_plugin = $this->db->query("SELECT * FROM " . $this->database_prefix . $this->plugins_table . " WHERE id='$id'");
        if ($this->db->num($res_plugin) > 0) {
            $data_plugin = $this->db->fetch_object($res_plugin);
            $result = '<?xml version="1.0" encoding="' . $this->xml_encoding . '"?>';
            $result .= "\r" . '<plugin>';
            $result .= "\r\t" . '<title>' . $data_plugin->titre . '</title>';
            $result .= "\r\t" . '<name>' . $data_plugin->name . '</name>';
            $result .= "\r\t" . '<version>' . $data_plugin->version . '</version>';
            $result .= "\r\t" . '<author>' . $data_plugin->author . '</author>';
            $result .= "\r\t" . '<description>' . $data_plugin->description . '</description>';
            if ($this->is_serialized($data_plugin->place)) {
                $places = unserialize($data_plugin->place);
                foreach ($places as $x => $place) {
                    $result .= "\r\t" . '<place>' . $place . '</place>';
                }
                unset($places);
            } else {
                $result .= "\r\t" . '<place>' . $data_plugin->place . '</place>';
            }
            $result .= "\r\t" . '<code>' . "\r\t\t" .
                    $this->cData("\r\t\t" . stripslashes($data_plugin->code) . "\r\t\t") .
                    "\r\t" . '</code>';
            $result .= "\r\t" . '<status>' . $data_plugin->status . '</status>';
            $res_plugins_settings = $this->db->query("SELECT * FROM " . $this->database_prefix . $this->plugins_settings_table . "
                                                    WHERE plugin_id='$id'");
            if ($this->db->num($res_plugins_settings) > 0) {
                $result .= "\r\t" . "<settings>";
                while ($data_plugin_setting = $this->db->fetch_object($res_plugins_settings)) {
                    $result .= "\r\t" . '<' . $data_plugin_setting->name . '>';
                    $result .= "\r\t\t" . '<text>' . $data_plugin_setting->text . '</text>';
                    $type = unserialize($data_plugin_setting->type);
                    if (in_array($type['type'], array('text', 'textarea', 'password'))) {
                        $result .= "\r\t\t" . '<type name="' . $type['type'] . '" />';
                    } else {
                        $result .= "\r\t\t" . '<type name="' . $type['type'] . '">';
                        foreach ($type['options'] as $s) {
                            $result .= "\r\t\t\t" . '<option value="' . $s['value'] . '">' . $s['content'] . '</option>';
                        }
                        $result .= "\r\t\t" . '</type>';
                    }
                    $result .= "\r\t\t" . '<valeur>' . $data_plugin_setting->valeur . '</valeur>';
                    $result .= "\r\t" . '</' . $data_plugin_setting->name . '>';
                }
                $result .= "\r\t</settings>";
            }
            $result .= "\r</plugin>";
            $result = trim($result);
            $plugin_file = basename($data_plugin->plugin_file);
            unset($data_plugin, $data_plugin_setting);
            $taille = strlen($result);
            header("Content-Type: application/force-download; name=\"$plugin_file\"");
            header("Content-Transfer-Encoding: binary");
            header("Content-Length: $taille");
            header("Content-Disposition: attachment; filename=\"$plugin_file\"");
            header("Expires: 0");
            header("Cache-Control: no-cache, must-revalidate");
            header("Pragma: no-cache");
            echo $result;
            ob_end_flush();
            exit();
        } else {
            return false;
        }
    }

    public function get_plugin($place) {
        global $Engine;
        $this->core = $Engine;
        $place = strip_tags(addslashes($place));
        $res_plugin = $this->db->query("SELECT code,place,id FROM {$this->plugins_table}
                                        WHERE status='1'");
        if ($this->db->resultCount() > 0) { // plugin_setting
            while ($data_plugin = $this->db->fetchObject()) {
                if ($this->is_serialized($data_plugin->place)) {
                    $places = unserialize($data_plugin->place);
                    if (in_array($place, $places)) {
                        eval("?>" . stripslashes($data_plugin->code));
                    }
                } else {
                    if ($place == $data_plugin->place) {
                        eval("?>" . stripslashes($data_plugin->code));
                    }
                }
            }
        }
        
    }

    public function runHooks($place){
        return $this->get_plugin($place);
    }

    public function get_plugin_settings($plg_name, $name) { //
        $res_plg = $this->db->query("SELECT id FROM " . $this->database_prefix . $this->plugins_table . "
                                    WHERE name='$plg_name'
                                    ");
        $plg_id = $this->db->fetchObject();
        $id = $plg_id->id;
        $res = $this->db->query("SELECT valeur FROM $this->plugins_settings_table
                                  WHERE name='$name' AND plugin_id='$id'");
        $data = $this->db->fetchObject();
        return $data->valeur;
    }

    public function delete_plugin($id) {
        if (!is_int($id)) {
            return false;
        }
        $id = intval(abs($id));
        $plugin_file = $this->get_plugin_file($id);
        $delete_plugin = $this->db->query("DELETE FROM $this->plugins_table WHERE id IN ('$id')");
        $delete_plugin = $this->db->query("DELETE FROM $this->plugins_settings_table WHERE plugin_id IN ('$id')");
        if (file_exists($plugin_file)) {
            //chmod($plugin_file, 0777);
            unlink($plugin_file);
        }
        return $delete_plugin;
    }

    public function delete_selected_plugins($array) {
        if (!is_array($array)) {
            return false;
        }
        $array = array_map('intval', $array);
        $imp_plugins = implode(" , ", $array);
        $this->delete_selected_plugins_files($array);
        $delete_plugin = $this->db->query("DELETE FROM " . $this->database_prefix . $this->plugins_table . " 
                                            WHERE id IN(" . $imp_plugins . ");
                                            DELETE FROM " . $this->database_prefix . $this->plugins_settings_table . " 
                                            WHERE plugin_id IN(" . $imp_plugins . ")");
        return $delete_plugin;
    }

    private function delete_selected_plugins_files($array) {
        $count = count($array);
        $result = array();
        for ($i = 0; $i < $count; $i++) {
            $result[$i] = $this->get_plugin_file($array[$i]);
            if (file_exists($result[$i])) {
                chmod($result[$i], 0777);
                unlink($result[$i]);
            }
        }
    }

    public function active_selected_plugins($array) {
        if (!is_array($array)) {
            return false;
        }
        $array = array_map('intval', $array);
        $imp_plugins = implode(" , ", $array);
        $delete_plugin = $this->db->query("UPDATE " . $this->database_prefix . $this->plugins_table . "
                                            SET status = 1
                                            WHERE id IN(" . $imp_plugins . ")");
        return $delete_plugin;
    }

    public function active_plugin($id) {
        if (!is_int($id)) {
            return false;
        }
        $id = intval(abs($id));
        $active_plugin = $this->db->query("UPDATE " . $this->database_prefix . $this->plugins_table . " SET 
                                            status=1 WHERE id='$id'");
        return $active_plugin;
    }

    public function desactive_plugin($id) {
        if (!is_int($id)) {
            return false;
        }
        $id = intval(abs($id));
        $desactive_plugin = $this->db->query("UPDATE " . $this->database_prefix . $this->plugins_table . " SET
                                                status=2 WHERE id='$id'");
        return $desactive_plugin;
    }

    public function desactive_selected_plugins($array) {
        if (!is_array($array)) {
            return false;
        }
        $array = array_map('intval', $array);
        $imp_plugins = implode(" , ", $array);
        $delete_plugin = $this->db->query("UPDATE " . $this->database_prefix . $this->plugins_table . "
                                            SET status = 2
                                            WHERE id IN(" . $imp_plugins . ")");
        return $delete_plugin;
    }

    /*
     * upload a plugin to plugins folder
     * @param : String input file name
     * @access : public
     */

    public function upload_plugin($files_name) {
        global $core;
        if (isset($_FILES[$files_name])) {
            $file_name = $_FILES[$files_name]['name'];
            $file_size = $_FILES[$files_name]['size'];
            $file_type = $_FILES[$files_name]['type'];
            $file_temp = $_FILES[$files_name]['tmp_name'];
            $file_ext = $this->get_extension($file_name);
            if (empty($file_name)) {
                echo error_msg("المرجو تحديد هاك من اجل الرفع");
                $core->html->redirect();
            } else if ($file_ext != "xml" or !in_array($file_type, $this->xml_mime_type)) {
                echo error_msg("الملف المحدد ليس بملف هاك");
                $core->html->redirect();
            } else {
                if (file_exists($this->plugins_folder . $file_name)) {
                    echo $core->html->error_msg("الهاك المحدد متوفر مسبقا");
                    $core->html->redirect();
                } else {
                    $upload = move_uploaded_file($file_temp, $this->plugins_folder . $file_name);
                    $this->import_plugin($this->plugins_folder . $file_name);
                    if (!$upload) {
                        return false;
                    } else {
                        return true;
                    }
                }
            }
        } else {
            return false;
        }
    }

    public function get_plugin_info($id, $field) {
        if (!is_int($id)) {
            return false;
        }
        $plugins_table = $this->database_prefix . $this->plugins_table;
        $field = strip_tags(addslashes($field));
        $res = $this->db->query("SELECT $field FROM $plugins_table WHERE id='$id'");
        if ($this->db->num($res) > 0) {
            $data = $this->db->fetch_object($res);
            return $data->$field;
        }
        return false;
    }

    public function has_settings($id) {
        if (!is_int($id)) {
            return false;
        }
        $plugins_setting_table = $this->database_prefix . $this->plugins_settings_table;
        $res = $this->db->query("SELECT id FROM " . $plugins_setting_table . " WHERE plugin_id='$id'");
        if ($this->db->num($res) > 0) {
            return true;
        }
        return false;
    }

    public function update_setting($id,$name,$value){
        global $Engine;
        $sql = $this->db->query("UPDATE plugins_settings SET valeur='$value' WHERE name='$name' AND plugin_id IN ('$id')");
        if ($sql) {
            echo $Engine->html->success_msg("Save setting success");
            $Engine->html->redirect();
        } else {
            echo $Engine->html->error_msg("save setting crach!");
            $Engine->html->redirect();
        }
    }

}
?>