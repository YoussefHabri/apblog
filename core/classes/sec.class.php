<?php
/**
 * Part of the APBlog Core.
 *
 * @package    APBlog
 * @version    2.0
 * @author     APBlog Development Team
 * @license    GNU/GPL License
 * @copyright  2012 APBlog Development Team
 * @link       http://apblog.arabstep.com (parnter Arabstep.com)
 */

class Sec
{
	public function xss_safe($var) {
        return strip_tags(addslashes(trim($var)));
    }

    public function valid_email($email) {
        $pattren = "/^[A-Z0-9_.-]{1,40}+@([A-Z0-9_-]){2,30}+\.([A-Z0-9]){2,20}$/i";
        if (preg_match($pattren, $email) && filter_var($email, FILTER_VALIDATE_EMAIL)) {
            return true;
        }
        return false;
    }

    public function valid_int($id) {
        if (!is_numeric($id) || empty($id) || !isset($id) ||
                !filter_var($id, FILTER_VALIDATE_INT)) {
            return false;
        } else {
            return (int) intval(abs(trim($id)));
        }
    }

    public function get_real_ip() {
        global $core;
        if (!empty($core->server['HTTP_CLIENT_IP'])) { //check ip from share internet
            $ip = $core->server['HTTP_CLIENT_IP'];
        } elseif (!empty($core->server['HTTP_X_FORWARDED_FOR'])) { //to check ip is pass from proxy
            $ip = $core->server['HTTP_X_FORWARDED_FOR'];
        } else {
            $ip = $core->server['REMOTE_ADDR'];
        }
        return $ip;
    }

    public function valid_url($url) {
        $pattren = "|^http(s)?://[a-z0-9-]+(.[a-z0-9-]+)*(:[0-9]+)?(/.*)?$|i";
        if (preg_match($pattren, $url) && filter_var($url, FILTER_VALIDATE_URL)) {
            return true;
        }
        return false;
    }

    function valid_ip($ip) {
        if (filter_var($ip, FILTER_VALIDATE_IP)) {
            return true;
        }
        return false;
    }
}