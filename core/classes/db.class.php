<?php
/**
 * Part of the APBlog Core.
 *
 * @package    APBlog
 * @version    2.0
 * @author     APBlog Development Team
 * @license    GNU/GPL License
 * @copyright  2012 APBlog Development Team
 * @link       http://apblog.arabstep.com (parnter Arabstep.com)
 */

// ------------------------------------------------------------------------

/**
 * db.class.php 
 *
 * The DataBase class
 *
 * @package		Omega Blog
 * @subpackage	Engine
 * @category	classes
 * @author		Omega Dev Team
 */

class Db
{

	/**
	* str content database server
	* @private	str
	*/
    private $_dbhost;
	/**
	* str content database username
	* @private	str
	*/
    private $_dblogin;
	/**
	* str content database password
	* @private	str
	*/
    private $_dbpass;
	/**
	* str content database name
	* @private	str
	*/
    private $_dbname;
	/**
	* str content database connection link
	* @private	bol
	*/
    private $_dblink;
	/**
	* str content database query
	* @private	bol
	*/
    private $_queryid;
	/**
	* array content list of error's
	* @public	array
	*/
    public  $_error = array();
	/**
	* array content result of fetch sql query
	* @public	array
	*/
    public  $_record = array();
    /**
	* object content result of fetch sql query
	* @public	object
	*/
    public  $_resObject;
	/**
	* int content mysql num rows
	* @public	int
	*/
    public  $_totalrecords;
	/**
	* int content last insert id
	* @public	int
	*/
    public  $_last_insert_id;



	/**
	* Constructor
	* @param	array  	db configuration var $config['db']
	* @return	void
	*/
 	public function __construct($config)
 	{
 		extract($config);
		$this->_dbhost         = $host;
		$this->_dblogin        = $user;
		$this->_dbpass         = $password;
		$this->_dbname         = $name;
		$this->_prefix         = $prefix;
		$this->_dblink         = NULL;
		$this->_queryid        = NULL;
		$this->_error          = array();
		$this->_record         = array();
		$this->_totalrecords   = 0;
		$this->_last_insert_id = 0;
 		
 		$this->query("SET NAMES '$encoding'");
		return true;
	}
	
	/**
	* Make database connection
	* @return	boll
	*/
    public function connection()
    {

		# if all required data not null
    	if(!is_null($this->_dbhost) OR !is_null($this->_dblogin)
    	   OR !is_null($this->_dbpass) OR !is_null($this->_dbname))
    	{

			# make  connection
            $this->_dblink = @mysql_pconnect($this->_dbhost, $this->_dblogin, $this->_dbpass);

	        if (!$this->_dblink)
	        {
	        	# mysql can't make  connection
	            $this->return_error(mysql_error());
	            return(FALSE);
	        }
	        else
	        {

				#select database to make connection
               $t = @mysql_select_db($this->_dbname, $this->_dblink);

		        if (!$t)
		        {
		        	#mysql can't select database
		            $this->return_error(mysql_error());
		            return(FALSE);
		        }
		        else
		        {

					# successfully connection
		        	return(TRUE);

		        }
	        }
    	}
    	else
    	{

			# same of required data is null
    		return(FALSE);
    	}


    }



	/**
	* disconnect database connection
	* @return	boll
	*/
    public function disconnect()
    {

        #close connection
        $close = @mysql_close($this->_dblink);

        if (!$close)
        {
            $this->return_error(mysql_error());

			#can't close connection
            return(FALSE);
        }
        else
        {

			unset($this->_dblink);

			#successfully disconnect
			return(TRUE);

        }

    }


	/**
	* set new error
	* @param	str  	error message
	* @return	error's array
	*/
    private function return_error($message)
    {

        return $this->_error[] = $message;

    }


	/**
	* FetchErrors
	* @return	error's array in success
	*/
    public function FetchErrors()
    {
    	# if isset error's
        if ($this->hasErrors())
        {
            reset($this->_error);

         	return($this->_error);

			# reset error array
            $this->resetErrors();
        }
        else
        {

			# no error's
        	return(FALSE);

        }

    }


	/**
	* Check if error's found
	* @return	boll
	*/
    public function hasErrors()
    {
        if (count($this->_error) > 0)
        {
            # we have error's
            return TRUE;
        }
        else
        {

    		# no error's
            return FALSE;
        }

    }



	/**
	* reset Error array
	* @return	boll
	*/
    public function resetErrors()
    {
        # if error's found
        if ($this->hasErrors())
        {
            unset($this->_error);
            $this->_error = array();

            return(TRUE);
        }
        else
        {

			# no error's found
        	return(FALSE);

        }

    }


	/**
	* Send a Mysql Query
	* @param	str  	mysql query
	* @return	query id in success
	*/
    public function query($sql)
    {

		# if NUll param
		if(!is_null($sql))
		{
			# if no connection
	        if (empty($this->_dblink))
	        {
	           # make database connection
	           $makecon = $this->connection();

	           if(!$makecon)
	           {

					# can't make database connection
	           		return(FALSE);

	           }

	        }

	        $this->_queryid = @mysql_query($sql, $this->_dblink);

	        # error in query
	        if (!$this->_queryid)
	        {
	            $this->return_error(mysql_error()."\n");
	        }
	        else
	        {

				#query Implemented
		        return $this->_queryid;

	        }

		}
		else
		{
			#  NUll param
			return(FALSE);
		}

    }



	/**
	* fetch a result row in array
	* @return	array  in success
	*/
    public function fetchRow()
    {

    	# if isset query
        if (isset($this->_queryid))
        {
            return $this->_record = @mysql_fetch_array($this->_queryid);
        }
        else
        {
        	# can't find query to fetch
            $this->return_error('الاستعلام غير متوفر');
            return(FALSE);
        }

    }


    /**
	* fetch a result row in object
	* @return	object  in success
	*/
    public function fetchObject()
    {

    	# if isset query
        if (isset($this->_queryid))
        {
            return $this->_resObject = @mysql_fetch_object($this->_queryid);
        }
        else
        {
        	# can't find query to fetch
            $this->return_error('الاستعلام غير متوفر');
            return(FALSE);
        }

    }


	/**
	* get Last Insert Id
	* @return	int
	*/

    public function fetchLastInsertId()
    {

		# if isset connection
    	if(isset($this->_dblink))
    	{

			# get last insert id
	        $this->_last_insert_id = @mysql_insert_id($this->_dblink);

	        if (!$this->_last_insert_id)
	        {
	            $this->return_error('غير قادر علي جلب الاي دي');
	        }
	        else
	        {
	        	return $this->_last_insert_id;
	        }

    	}
    	else
    	# no connection found
    	{
    		return(FALSE);
    	}

    }


	/**
	* get number of rows in result
	* @return	int
	*/
    public function resultCount()
    {

		# if isset query
        if (isset($this->_queryid))
        {

	        $this->_totalrecords = @mysql_num_rows($this->_queryid);

	        return $this->_totalrecords;

        }
        else
        {
        	#no query
            $this->return_error('الاستعلام غير متوفر');
            return(FALSE);
        }

    }


	/**
	* Check if result found
	* @return	boll
	*/
    public function resultExist()
    {
        if (isset($this->_queryid) && ($this->resultCount() > 0))
        {
            return TRUE;
        }
        else
        {
			return FALSE;
        }
    }



	/**
	* free esult memory
	* @return	boll
	*/
    public function clear($result = 0)
    {
        if ($result != 0)
        {
            $t = @mysql_free_result($result);

            if (!$t)
            {
                $this->return_error('غير قادر علي تفريغ الذاكرة');
                return(FALSE);
            }
            else
            {
            	return(TRUE);
            }

        }
        else
        {
            if (isset($this->_queryid))
            {
                $t = @mysql_free_result($this->_queryid);

                if (!$t)
                {
                    $this->return_error('غير قادر علي تفريغ الذاكرة');
                    return(FALSE);
                }
                else
                {

					return(TRUE);

                }

            }
            else
            {
                $this->return_error('لم تقم باختيار استعلام ليتم تفريغه');

                return(FALSE);
            }

        }

    }


	/**
	* INSERT statment
	* @param	str  	table name
	* @param	array
	* @return	boll
	*/
	public function insert($info,$data)
	{

		if(is_array($data) AND !is_null($data))
		{

			$fld_names = implode(',', array_keys($data)).',';
			$fld_values = '\''.implode('\',\'', array_values($data)).'\',';

			$sql = 'INSERT INTO '.$info.'('.substr_replace($fld_names, '',-1,1).') VALUES('.substr_replace($fld_values, '',-1,1).')';
			$this->query($sql);

			return(TRUE);
		}
		else
		{

			return(FALSE);

		}
	}


	/**
	* UPDATE statment
	* @param	array  	table info
	* @param	array
	* @return	boll
	*/
	public function update($info,$data)
	{

        if(is_array($info) AND is_array($data))
        {
			$sql_set = '';

			foreach($data as $k=>$v)
					 	$sql_set .= ',`'.$k.'`=\''.addslashes($v).'\'';

			$query = 'UPDATE `'.$info['table'].'` SET '.substr($sql_set,1).' WHERE `'.$info['key'].'` =\''.$info['val'].'\'';

			$this->query($query);

			return(TRUE);

		}
		else
		{

			return(FALSE);

		}
	}



	public function __destruct ()
	{
	     $this->_dbhost 		= NULL;
	     $this->_dblogin 		= NULL;
	     $this->_dbpass 		= NULL;
	     $this->_dbname 		= NULL;
	     $this->_dblink 		= NULL;
	     $this->_queryid 		= NULL;
	     $this->_error 			= NULL;
	     $this->_record 		= NULL;
	     $this->_totalrecords 	= NULL;
	     $this->_last_insert_id = NULL;
 	}

}

?>
