<?php
// This is the parent class for all plugins
// It contains a private constructor, that
// basically makes sure we won't have any
// instances of our static classes. Making
// them completely static.
class plugin{
  private function __construct(){}
}

// This is the actual plugin class.
class pluginClass{  
  // This will be the list of active plugins
  static private $plugins = array();

  // Again, we don't want any instances 
  // of our static class.
  private function __construct(){}

  static function initialize(){
   // I have those variables elsewhere in
   // a config.php file. you can replace
   // those with your own
   global $config_fullpath;
   global $config_username;
   global $config_password;
   global $config_server;
   global $config_database;
   
   $list = array();   
   // Populate the list of directories to check against
   if ( ($directoryHandle = opendir( $config_fullpath . '/plugins/' )) == true ) {
       while (($file = readdir( $directoryHandle )) !== false) {
        // Make sure we're not dealing with a file or a link to the parent directory
           if( is_dir( $config_fullpath . '/plugins/' . $file ) && ($file == '.' || $file == '..') !== true )
            array_push( $list, $file );
       }
   }
   
   
   // Get the plugin list from MySQL. Note that you will have to replace
   // this code with your own, since I'm using classes that I didn't 
   // include in this article. Fortunately, it won't be too much of a problem.
   
   // Connect to mysql
   $mysqlConnection = new mysqlConnection( $config_username, $config_password, $config_server, $config_database );
   // We select all the plugins from our database
   // Each plugin has it's name stored, and whether
   // it is active or not (active = 0 or 1)
   $mysqlConnection->prepareQuery(  'SELECT * FROM plugins' );
   $results = $mysqlConnection->executeQuery();
   $results = $mysqlConnection->resultToObjects( $results );
   $newResult = array();
   
   
   
   // Create an array: 'plugin name' = 'active' (1 or 0)
   foreach ( $results as $result ){
    $newResult[$result->name] = $result->active;
   }   
   
   // Register the active plugins
   foreach( $list as $plugin ){
    if($newResult[$plugin] == "1"){
     pluginClass::register( $plugin );
    }
   }
  }

  // Hook the active plugins at a checkpoint. 
  // You will see exactly how it works later on.
  static function hook( $checkpoint ){
   // Cycle through all the plugins that are active
   foreach(pluginClass::$plugins as $plugin){
    if(!call_user_func( array( $plugin, $checkpoint ) ))
     // Throw an exception if we can't hook the plugin
     throw new Exception( "Cannot hook plugin ($plugin) at checkpoint ($checkpoint)" );
   }
  }

  // Registration adds the plugin to the list of plugins, and also
  // includes it's code into our runtime.
  static function register( $plugin ){
   global $config_fullpath;
   require_once( $config_fullpath . "/plugins/$plugin/$plugin.class.php" );   
   array_push( pluginClass::$plugins, $plugin );
  }
}