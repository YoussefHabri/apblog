<?php
/**
 * Part of the APBlog Core.
 *
 * @package    APBlog
 * @version    2.0
 * @author     APBlog Development Team
 * @license    GNU/GPL License
 * @copyright  2012 APBlog Development Team
 * @link       http://apblog.arabstep.com (parnter Arabstep.com)
 */

class Posts {
	var $_;
	var $data = array('content'=>'xss');

	function __construct(){
	}

	static function addAction($where,$function)
	{
		global $_;
		$where = (!empty($where) ? $where : "content");
		$val = $_->posts->data[$where];
		$func = call_user_func($function,$val);
		return $func;

	}

	function numPosts($type = 'post')
	{
		global $_;
		$_->db->query("SELECT * FROM posts WHERE post_type='$type' AND post_stat='publish'");
		return $_->db->resultCount();
	}

	function getInfo($post_id,$row)
	{
		global $_;
		$_->db->query("SELECT * FROM posts WHERE post_id IN ('$post_id')");
		$res = $_->db->fetchRow();
		return $res['post_thumbnail'];
	}

	function numPostsByCat($id)
	{
		global $_;
		$_->db->query("SELECT * FROM posts WHERE post_parent='$id'");
		return $_->db->resultCount();
	}

	function catByID($id)
	{
		global $_;
		$_->db->query("SELECT * FROM posts WHERE post_id='$id' and post_type ='cat' LIMIT 1");
		return $_->db->fetchRow();
	}

	function insert($data = array())
	{
		global $_;

		$keys = '';
		$vals = '';
		foreach($data as $key => $val){
			$keys .= "".$key.", ";
			$vals .= "'".$val."', ";
		}
		$keys = trim($keys,', ');
		$vals = trim($vals,', ');

		$sql = "INSERT INTO posts (".$keys.") VALUES (".$vals.")";
		$sql = $_->db->query($sql);
		die(mysql_error());
		if(!$sql){ $this->data['error'] = '1'; }
	}

}
$posts = new Posts();