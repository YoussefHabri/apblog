<?php

// define constants
if(!defined('DEFAULT_LOCALE')){
	define('DEFAULT_LOCALE', 'ar_MA');
}

T_setlocale(LC_MESSAGES, DEFAULT_LOCALE);
global $_;
if($_->info->get_data('current_page') == 'admin'){
	$domain = 'admin';
	define('LOCALE_DIR', APP.DS.'locale');
}else{
	$domain = 'language';
	$theme  = $_->info->get_info('default_template');
	define('LOCALE_DIR', THEMES_PATH.DS.$theme.DS.'langs');
}
T_bindtextdomain($domain, LOCALE_DIR);
T_bind_textdomain_codeset($domain, 'utf-8');
T_textdomain($domain);