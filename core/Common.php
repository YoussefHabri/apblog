<?php
/**
 * Part of the APBlog Core.
 *
 * @package    APBlog
 * @version    2.0
 * @author     APBlog Development Team
 * @license    GNU/GPL License
 * @copyright  2012 APBlog Development Team
 * @link       http://apblog.arabstep.com (parnter Arabstep.com)
 */

// Load Core
require_once CORE.DS.'Engine.php';

// System functions
require_once CORE.DS.'functions.php';

// General template functions
require_once CORE.DS.'general-template.php';

if(!defined('IN_ADMIN')) {
	// Load system composent
	require_once CORE.DS.'loadsystems.php';
}