<?php
/**
 * Part of the APBlog Core.
 *
 * @package    APBlog
 * @version    2.0
 * @author     APBlog Development Team
 * @license    GNU/GPL License
 * @copyright  2012 APBlog Development Team
 * @link       http://apblog.arabstep.com (parnter Arabstep.com)
 */

function get_url() {

	$con_type = (($_SERVER['SERVER_PORT'] == '80') ? 'http://' : 'https://');
	$http_url = $con_type.$_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI'];
	$http_url = trim($http_url,'/');
	$db_url = bloginfo('site_url');
	$url = (!empty($db_url) ? $db_url : $http_url);

	return $url;
}

function siteurl() {
	return get_url();
}

function bloginfo($var) {
	return Info::get($var);
}

function __($index) {
	global $_;
	$var = strtolower($index);
	$var = str_ireplace(' ', '_', $var);
	if(isset($_->phrase[$var]) && !empty($_->phrase[$var])){
		return $_->phrase[$var];
	}else{
		return $index;
	}
}

function _e($index) {
	echo __($index);
}

function have_post() {
	global $_;
	$num = $_->posts->numPosts('post');
	if($num > 0) {
		return true;
	}else{
		return false;
	}
}
function blog_post(){
	global $_;
	return $_->posts->postsList('post');
}

function blog_cats($ul_class,$active_class) {
	global $_;
	$_->db->query("SELECT * FROM posts WHERE post_type='cat'");
	echo '<ul class="'.$ul_class.'">';
	while($cat = $_->db->fetchObject()) {
		$num = $_->posts->numPostsByCat($cat->post_id);
		$active = !empty($active_class) ? $active_class : null;
		if($num > 0) {
			echo '<li><a href="{$url}" '.$active.'>'.$cat->title.'</a></li>';
		}
	}
}

function blog_pages($ul_class,$active_class) {
	global $_;
	$_->db->query("SELECT * FROM posts WHERE post_type='page'");
	echo '<ul class="'.$ul_class.'">';
	while($page = $_->db->fetchObject()) {
		$num = $page->active;
		$active = !empty($active_class) ? $active_class : null;
		if($num > 0) {
			echo '<li><a href="#name" '.$active.'>'.$page->title.'</a></li>';
		}
	}
	echo '</ul>';
}

function blog_pages_li($active_class) {
	global $_;
	$_->db->query("SELECT * FROM posts WHERE post_type='page'");
	while($page = $_->db->fetchObject()) {
		$num = $page->active;
		$active = !empty($active_class) ? $active_class : null;
		if($num > 0) {
			echo '<li><a href="#name" '.$active.'>'.$page->title.'</a></li>';
		}
	}
}

function author($id)
{
	global $_;
	$u = $_->info->users($id);
	if(!empty($u['first_name']) && !empty($u['last_name'])){
		return $u['first_name'].' '.$u['last_name'];
	}else{
		return $u['username'];
	}
}

function category($id)
{
	global $_;
	$c = $_->posts->catByID($id);
	return $c['post_title'];
	
}

function xss_safe($var) {
    return strip_tags(addslashes(trim($var)));
}

function valid_email($email) {
    $pattren = "/^[A-Z0-9_.-]{1,40}+@([A-Z0-9_-]){2,30}+\.([A-Z0-9]){2,20}$/i";
    if (preg_match($pattren, $email) && filter_var($email, FILTER_VALIDATE_EMAIL)) {
        return true;
    }
    return false;
}

function valid_int($id) {
    if (!is_numeric($id) || empty($id) || !isset($id) ||
            !filter_var($id, FILTER_VALIDATE_INT)) {
        return false;
    } else {
        return (int) intval(abs(trim($id)));
    }
}

function get_real_ip() {
    global $core;
    if (!empty($core->server['HTTP_CLIENT_IP'])) { //check ip from share internet
        $ip = $core->server['HTTP_CLIENT_IP'];
    } elseif (!empty($core->server['HTTP_X_FORWARDED_FOR'])) { //to check ip is pass from proxy
        $ip = $core->server['HTTP_X_FORWARDED_FOR'];
    } else {
        $ip = $core->server['REMOTE_ADDR'];
    }
    return $ip;
}

function valid_url($url) {
    $pattren = "|^http(s)?://[a-z0-9-]+(.[a-z0-9-]+)*(:[0-9]+)?(/.*)?$|i";
    if (preg_match($pattren, $url) && filter_var($url, FILTER_VALIDATE_URL)) {
        return true;
    }
    return false;
}

function valid_ip($ip) {
    if (filter_var($ip, FILTER_VALIDATE_IP)) {
        return true;
    }
    return false;
}