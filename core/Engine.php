<?php
/**
 * Part of the APBlog Core.
 *
 * @package    APBlog
 * @version    2.0
 * @author     APBlog Development Team
 * @license    GNU/GPL License
 * @copyright  2012 APBlog Development Team
 * @link       http://apblog.arabstep.com (parnter Arabstep.com)
 */

class Engine {
	var $Eversion  = '2.0';
	var $Ecodename = 'Arch Age';
	var $Edev_stat = '#alpha1';

	var $config = array();

	function __construct()
	{
		$this->loadConfigs();

		$this->loadClass('Db','',$this->config['db']);
		$this->loadClass('Info');
		$this->loadClass('Plugins');
		$this->loadClass('Posts','',$this);
		$this->loadClass('uFlex','users');
		$this->loadClass('version');

		$this->loadLang();
	}

	/**
	 * Loads a class and assign it to a var in the main class (Engine).
	 *
	 * @param   string  $name
	 * @param   string  $obj
	 * @param   string  $assign
	 * @return  object
	 */
	function loadClass($name, $obj='', $assign='')
	{
		$s_name = (!empty($obj) ? $obj : strtolower($name));
		$assign = (empty($assign) ? $this : $assign);
		if(file_exists(CORE.DS.'classes'.DS.$s_name.'.class.php'))
		{
			include_once CORE.DS.'classes'.DS.$s_name.'.class.php';
			return $this->$s_name = new $name($assign);
		}
	}

	/**
	 * Load a configs file. If not defined by the user, use default.
	 *
	 * @param   string  $path
	 * @return  void
	 */
	function loadConfigs($path = null)
	{
		$path = (!empty($path) ? $path : APP.DS.'config.php');
		include($path);
		$this->config = $config;
		return true;
	}

	/**
	 * Load a language file. If not defined by the user, use default.
	 *
	 * @return  array
	 */
	function loadLang() {
		if(!defined('DEFAULT_LANG')) {
			define('DEFAULT_LANG', 'ar');
			$lang = DEFAULT_LANG.DS.'language';
		}
		if($this->info->where() == 'admin') {
			$lang = DEFAULT_LANG.DS.'language_admin';
		}

		$path = APP.DS.'locale'.DS.$lang.'.php';
		return $this->phrase = include $path;
	}
}

$_ = new Engine();
$Engine = &$_;