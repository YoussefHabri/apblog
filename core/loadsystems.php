<?php
/**
 * Part of the APBlog Core.
 *
 * @package    APBlog
 * @version    2.0
 * @author     APBlog Development Team
 * @license    GNU/GPL License
 * @copyright  2012 APBlog Development Team
 * @link       http://apblog.arabstep.com (parnter Arabstep.com)
 */
$pager = $_->loadClass('Pager');
$tpl   = $_->loadClass('RainTpl','','tpl');

function where()
{
	global $_;
	return $_->info->where();
}

function get_content($where)
{
	return get_template_part($where);
}

function set_current_page($page = 'index') {
	global $_;
	return $_->info->set_data('current_page',$page);
}
/*
function runAll()
{
	global $_;
	$_->plugins->loadActive();

	set_current_page(where());

	get_template_part('functions');

	$homepage = Info::get('homepage');
	if(isset($homepage))
	{
		$page = $_->posts->get_page_id($homepage);
		get_content('page-'.$page['template_name']);
	}else{
		get_content(where());
	}

}
*/

function ConfTPL()
{
	global $_;
	$tpl = $_->loadClass('RainTpl','','tpl');
	$default_template = bloginfo('default_template');
	$template_dir     = THEMES_PATH.DS.$default_template.DS;
	$compile_dir      = THEMES_PATH.DS.$default_template.DS.'cache'.DS;

	$config = array( 
					"base_url"	=> null, 
					"tpl_dir"	=> $template_dir,
					"cache_dir"	=> $compile_dir,
					"tpl_ext"   => 'php',
					"debug"		=> true,
				   );
	//use Rain;
	RainTpl::configure( $config );
}

function MainAssign()
{
	global $_;
	$tpl = &$_->tpl;

	$assign = array();

	$tpl->view('',$assign);
}

function _home(){
	global $_,$tpl;

	$PerPage = $_->info->get('in_page');
	$_->pager->SetPerPage($PerPage);
	$_->pager->SetPageUrl("index.php");

	$sql = $_->pager->CreatePaginator("SELECT * FROM posts WHERE post_type = 'post' AND post_stat IN ('publish') ORDER by post_date DESC");

	$assign = array(
		'post_sql' => $sql,
		'post_num' => $_->posts->numPosts(),
		'posts'    => $_->posts
		);
	$tpl->view('index',$assign);
}

function _cat($cat_id = null){
	global $_;

	//$cat_id = (isset($_GET['id']) ? $_GET['id'] : "");

	$PerPage = $_->info->get('in_page');
	$_->pager->SetPerPage($PerPage);
	$_->pager->SetPageUrl("index.php?cat_id=".$cat_id);

	$sql = $_->pager->CreatePaginator("SELECT * FROM posts WHERE cat_id IN ('$cat_id') AND post_type IN ('post') AND publish IN ('active') ORDER by date DESC");

	$assign = array(
		'post_sql' => $sql,
		'post_num' => $_->cats->numPosts($cat_id),
		'posts'    => $_->posts
		);
	$_->tpl->view('category',$assign);
}

function _author($author = null){
	global $_;

	//$cat_id = (isset($_GET['id']) ? $_GET['id'] : "");

	$PerPage = $_->info->get('in_page');
	$_->pager->SetPerPage($PerPage);
	$_->pager->SetPageUrl("index.php?author=".$author);

	$sql = $_->pager->CreatePaginator("SELECT * FROM posts WHERE author_id IN ('$author') AND post_type IN ('post') AND publish IN ('active') ORDER by date DESC");

	$assign = array(
		'post_sql' => $sql,
		'post_num' => $_->posts->numPostsbyAuthor($author),
		'posts'    => $_->posts
		);
	_::view('post_author',$assign);
}

function _post(){
	global $_;

	$id = (isset($_GET['id']) ? $_GET['id'] : "");
	$_->posts->set_id($id);
	$assign = array(
		'post' => $_->posts->data,
		);
	_::view('single',$assign);
}

function _page(){
	global $_;

	$id = (isset($_GET['page_id']) ? $_GET['page_id'] : "");
	$_->posts->set_id($id);
	$assign = array(
		'post' => $_->posts->data,
		);
	_::view('page',$assign);
}

function _search($trm = null){
	global $_;

	$s = (!empty($_GET['s']) ? $_GET['s'] : $trm);

	$_->search->settable("posts");
	$_->search->setidentifier('id');
	$results_array = $_->search->find($s);

	$assign = array(
		'posts'      => $_->posts,
		'_'     => $_,
		'post_array' => $results_array,
		'lang'       => $lang,
		);
	_::view('search',$assign);
}

function _404(){
	global $_;

	$assign = array(
		//'blocks' => $_->html->GetBlocks(),
		);
	_::view('404',$assign);
}

function _feed(){
	global $_;

	include DIR.DS.'includes'.DS.'rss_generator.php';

}

function runAll()
{
	global $_;
	ConfTPL();

	extract($_GET);

	if(isset($id) && !empty($id)){
		$_->posts->update_views();
		$sql = $_->db->query("SELECT * FROM posts WHERE post_id IN ('$id') AND post_type IN ('post') AND publish IN ('active') LIMIT 1");
		$num = $_->db->resultCount();

		if($num > 0){
			echo _post();
		}else{
			echo $_->html->redirect('','0');
		}
	}elseif(isset($page_id) && !empty($page_id)){
		$id = $page_id;
		$_->posts->update_views();
		$sql = $_->db->query("SELECT * FROM posts WHERE id IN ('$id') AND post_type IN ('page') AND publish IN ('active') LIMIT 1");
		$num = $_->db->resultCount();

		if($num > 0){
			echo _page();
		}else{
			echo $_->html->redirect('','0');
		}
	}elseif(isset($cat_id) && !empty($cat_id)){
		echo _cat($cat_id);
	}elseif(isset($author) && !empty($author)){
		echo _author($author);
	}elseif(isset($s) && !empty($s)){
		echo _search($s);
	}elseif(!empty($_GET) && !isset($s) && !isset($p)){
		echo _404();
	}else{
		echo _home();
	}

}
runAll();