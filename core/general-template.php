<?php
/**
 * Part of the APBlog Core.
 *
 * @package    APBlog
 * @version    2.0
 * @author     APBlog Development Team
 * @license    GNU/GPL License
 * @copyright  2012 APBlog Development Team
 * @link       http://apblog.arabstep.com (parnter Arabstep.com)
 */


/**
 * General template tags that can go anywhere in a template.
 *
 * @package Engine
 */

function load_template_file($name) {

	$default_template = Info::get('default_template');
	$tpl_path = THEMES_PATH.DS.$default_template.DS.$name;

	if(file_exists($tpl_path)) {
		include $tpl_path;
	}else{
		include APP.DS.'defaults'.DS.$name;
	}
}

function get_template_url() {
	$site_url = get_url();
	$root = dirname($_SERVER['SCRIPT_FILENAME']).'/';
	$theme = str_ireplace('\\', '/', THEMES_PATH);
	$theme_dir = str_ireplace($root,'',$theme);
	$default_template = $theme_dir.'/'.bloginfo('default_template');
	$template_url = $site_url.'/'.$default_template;

	return $template_url;
}

function get_file_template_url($name) {

	$default_template_url = get_template_url();
	$tpl_path = $default_template_url.'/'.$name;

	return $tpl_path;
}

/**
 * Load template.
 *
 * Includes the template for a theme.
 *
 * @since 1.0.0
 *
 * @param string $name The name of the specialised header.
 */
function load_template( $tpl = array() ) {

	foreach ($tpl as $key => $value) {
		return load_template_file($value);
	}

}

/**
 * Load header template.
 *
 * Includes the header template for a theme or if a name is specified then a
 * specialised header will be included.
 *
 * For the parameter, if the file is called "header-special.php" then specify
 * "special".
 *
 * @since 1.0.0
 *
 * @param string $name The name of the specialised header.
 */
function get_header( $name = null ) {

	$templates = array();

	$templates[] = 'header.php';
	if ( isset($name) )
		$templates[] = "header-{$name}.php";

	return load_template($templates);
}

/**
 * Load footer template.
 *
 * Includes the footer template for a theme or if a name is specified then a
 * specialised footer will be included.
 *
 * For the parameter, if the file is called "footer-special.php" then specify
 * "special".
 *
 * @uses locate_template()
 * @since 1.0.0
 *
 * @param string $name The name of the specialised footer.
 */
function get_footer( $name = null ) {

	$templates = array();
	
	$templates[] = 'footer.php';
	if ( isset($name) )
			$templates[] = "footer-{$name}.php";

	return load_template($templates);
}

/**
 * Load sidebar template.
 *
 * Includes the sidebar template for a theme or if a name is specified then a
 * specialised sidebar will be included.
 *
 * For the parameter, if the file is called "sidebar-special.php" then specify
 * "special".
 *
 * @uses locate_template()
 * @since 1.0.0
 *
 * @param string $name The name of the specialised sidebar.
 */
function get_sidebar( $name = null ) {

	if(template_info('sidebar')) {
		$templates = array();

		$templates[] = 'sidebar.php';
		if ( isset($name) )
			$templates[] = "sidebar-{$name}.php";

		return load_template($templates);
	}else{
		return false;
	}
}

/**
 * Load a template part into a template
 *
 * @since 1.0.0
 *
 * @param string $slug The slug name for the generic template.
 * @param string $name The name of the specialised template.
 */
function get_template_part( $slug, $name = null ) {

	$templates = array();

	$templates[] = $slug.'.php';
	if ( isset($name) )
		$templates[] = $slug."-".$name.".php";

	return load_template($templates);
	
}

function get_template_info($var) {
	$default_template = bloginfo('default_template');
	$template_path = THEMES_PATH.DS.$default_template;
    if (file_exists($template_path . "/info.ini")) {
        $content = parse_ini_file($template_path . "/info.ini");
        return $content[$var];
    } else {
        return false;
    }
}

function template_info($var) {
	return get_template_info($var);
}

function stylesheet() {
	return get_file_template_url('style.css');
}

function get_thumbnail($post_thumbnail){
	$path = get_url().'/apps/data/thumbnail/';
	return $path.$post_thumbnail;
}