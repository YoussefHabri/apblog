		<div id="content">
			<h2>Soft and easy <span class="black">colors!</span></h2>
			<p>
			This design is supposed to be a Blog template, but can be used for
			another purpose, such as a small personal or business website.
			If you need a PHP and MySQL script to display news on your site, 
			visit <a href="http://www.solucija.com">Solucija</a>, and download 
			your free copy of s}news, it's easy to integrate into this design, 
			or can be a simple content management system on it's own. Offcourse, 
			you are free to use this template in any way you want, no link back 
			or copyright notice is required.
			</p>
			
			<p><a href="#">Read more</a> > Posted on: August 7th, 2005</p>

			<h2>Nice <span class="black">blog</span> template!</h2>
			<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy 
			nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut 
			wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit 
			lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure 
			dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore 
			eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui 
			blandit praesent luptatum zzril delenit a...</p>
			
			<p><a href="#">Read more</a> > Posted on: June 2nd, 2005</p>

			<h2>Lorem ipsum <span class="black">text</span></h2>
			<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy 
			nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut 
			wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit 
			lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure 
			dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore 
			eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui 
			blandit praesent luptatum zzril delenit a...</p>
			
			<p><a href="#">Read more</a> > Posted on: April 24th, 2005</p>
			
		</div>