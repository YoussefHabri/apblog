<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en" xml:lang="en">
<head>
	<title><?php echo bloginfo('site_title'); ?></title>
	<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
	<link rel="stylesheet" type="text/css" href="<?php echo get_file_template_url('style.css'); ?>" />
</head>
<body>
	<div id="container">
		<div id="header">
			<a href="<?php echo siteurl(); ?>"><?php echo bloginfo('site_title'); ?></a>
			<h2><?php echo bloginfo('site_description'); ?></h2>
		</div>
		
		<div id="horizontal">
			<a href="#"><?=__('home')?></a>
			<a href="#">Articles</a>
			<a href="#">Forum</a>
			<a href="#">Gallery</a>
			<a href="#">Our Affiliates</a>
			<a href="#">Support</a>
			<a href="#">Contact</a>
		</div>