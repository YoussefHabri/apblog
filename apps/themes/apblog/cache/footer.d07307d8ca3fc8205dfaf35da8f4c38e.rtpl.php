<?php if(!class_exists('RainTpl')){exit;}?>		</div>

    </div> <!-- /container -->

    <!-- Le javascript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="<?php echo get_template_url(); ?>/assets/js/jquery.js"></script>
    <script src="<?php echo get_template_url(); ?>/assets/js/bootstrap.min.js"></script>
    <script src="<?php echo get_template_url(); ?>/assets/js/main-app.js"></script>

  </body>
</html>
