<?php if(!class_exists('RainTpl')){exit;}?><?php $tpl = new RainTpl;$tpl_dir_temp = static::$conf['tpl_dir'];$tpl->assign( $this->var );$tpl->draw( dirname("header") . ( substr("header",-1,1) != "/" ? "/" : "" ) . basename("header") );?>
		    <div class="span8">
          <?php  if(have_post()):  ?>
          <?php $counter1=-1; if( is_array($post_sql) && sizeof($post_sql) ) foreach( $post_sql as $key1 => $value1 ){ $counter1++; ?>
            <div class="post">
                <a href="#" class="thumbnail"><img src="<?php echo get_thumbnail($value1->post_thumbnail); ?>" alt=""></a>
                <div class="caption well">
                  <h4><?php echo $value1->post_title; ?></h4>
                  <h6>Author: <?php echo author($value1->post_author_id); ?>, 
                    Category: <?php echo category($value1->post_parent); ?>, 
                    Date: <?php echo date("Y.m.d H:m",$value1->post_date); ?></h6>
                  <p><?php echo $value1->post_content; ?></p>
                  <p>
                    <div class="btn-group pull-left">
                      <a class="btn" href="#comments"><i class="icon-comment"></i> <?php _e('Comments'); ?></a>
                      <a class="btn" href="#like"><i class="icon-thumbs-up"></i> <?php _e('Like'); ?></a>
                    </div>
                    <div class="btn-group pull-right">
                      <a class="btn" href="#readmore"><i class="icon-plus"></i> <?php _e('Read More'); ?></a>
                      
                    </div>
                    <br>
                  </p>
                </div>
            </div>
            <hr>
            <?php } ?>
          <?php  else:  ?>
            <blockquote>
              <p>&lt;?_e('404 Error!')?&gt;</p>
              <small>&lt;?_e('by Admin')?&gt;</small>
            </blockquote>
          <?php  endif;  ?>
        </div>
<?php $tpl = new RainTpl;$tpl_dir_temp = static::$conf['tpl_dir'];$tpl->assign( $this->var );$tpl->draw( dirname("sidebar") . ( substr("sidebar",-1,1) != "/" ? "/" : "" ) . basename("sidebar") );?>

<?php $tpl = new RainTpl;$tpl_dir_temp = static::$conf['tpl_dir'];$tpl->assign( $this->var );$tpl->draw( dirname("footer") . ( substr("footer",-1,1) != "/" ? "/" : "" ) . basename("footer") );?>