{include="header"}
		    <div class="span8">
          {php} if(have_post()): {/php}
          {loop="$post_sql"}
            <div class="post">
                <a href="#" class="thumbnail"><img src="{function="get_thumbnail($value->post_thumbnail)"}" alt=""></a>
                <div class="caption well">
                  <h4>{$value->post_title}</h4>
                  <h6>Author: {function="author($value->post_author_id)"}, 
                    Category: {function="category($value->post_parent)"}, 
                    Date: {function="date("Y.m.d H:m",$value->post_date)"}</h6>
                  <p>{$value->post_content}</p>
                  <p>
                    <div class="btn-group pull-left">
                      <a class="btn" href="#comments"><i class="icon-comment"></i> {e.Comments}</a>
                      <a class="btn" href="#like"><i class="icon-thumbs-up"></i> {e.Like}</a>
                    </div>
                    <div class="btn-group pull-right">
                      <a class="btn" href="#readmore"><i class="icon-plus"></i> {e.Read More}</a>
                      
                    </div>
                    <br>
                  </p>
                </div>
            </div>
            <hr>
            {/loop}
          {php} else: {/php}
            <blockquote>
              <p><?_e('404 Error!')?></p>
              <small><?_e('by Admin')?></small>
            </blockquote>
          {php} endif; {/php}
        </div>
{include="sidebar"}

{include="footer"}