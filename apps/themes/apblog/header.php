<!DOCTYPE html>
<html lang="ar">
  <head>
    <meta charset="utf-8">
    <title>{function="bloginfo('site_title')"}</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="{function="bloginfo('site_desc')"}">
    <meta name="generator" content="Advanced Press Blog">

    <!-- Le styles -->
    <link href="{function="get_template_url()"}/assets/css/bootstrap.css" rel="stylesheet">
    <link href="{function="get_template_url()"}/assets/css_rtl/bootstrap-rtl.min.css" rel="stylesheet">
    <link href="{function="get_template_url()"}/assets/css_rtl/bootstrap-responsive-rtl.min.css" rel="stylesheet">
    <link href="{function="get_template_url()"}/assets/css_rtl/rtl.css" rel="stylesheet">
    <link href="{function="get_file_template_url('style.css')"}" rel="stylesheet">

    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    <!-- Le fav and touch icons -->
    <link rel="shortcut icon" href="{function="get_template_url()"}/assets/ico/favicon.ico">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="{function="get_template_url()"}/assets/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="{function="get_template_url()"}/assets/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="{function="get_template_url()"}/assets/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="{function="get_template_url()"}/assets/ico/apple-touch-icon-57-precomposed.png">
  </head>

  <body>

    <div class="navbar navbar-fixed-top">
      <div class="navbar-inner">
        <div class="container">
          <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </a>
          <a class="brand" href="#">{function="bloginfo('site_title')"}</a>
          <div class="nav-collapse">
            <ul class="nav">
              <li class="active"><a href="#">{e.Home}</a></li>
              {function="blog_pages_li('active')"}
            </ul>
          </div><!--/.nav-collapse -->
        </div>
      </div>
    </div>

    <div class="container">

      <div class="subnav">
        {function="blog_cats('nav nav-pills','active')"}
      </div>

        <br>

      <div class="row-fluid">