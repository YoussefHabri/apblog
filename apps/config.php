<?php

/* database host */
$config['db']['host']     = 'localhost';

/* database name */
$config['db']['name']     = 'omega_v2.0';

/* database user name */
$config['db']['user']     = 'root';

/* database password */
$config['db']['password'] = '';

/* database encoding system for database operations */
$config['db']['encoding'] = 'utf8';

/* database prefix */
$config['db']['prefix']   = '';

return ($config);