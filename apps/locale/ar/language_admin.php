<?php

return array(
	'_admin_copyright'                   => '&copy; 2012, Advanced Press Blog',
	
	'dir'                                => 'rtl',
	'unicode' 							 => 'utf-8',
	'align'                              => 'right',
	'home'                               => 'الرئيسية',
	
	'_admin_login'                       => 'تسجيل الدخول',
	'_admin_login_username'              => 'المستخدم',
	'_admin_login_password'              => 'كلمة المرور',
	'_admin_login_submit'                => 'تسجيل الدخول',
	'_admin_login_error'                 => 'عفواً هناك خطأ بتسجيل الدخول',
	
	// sysinfo -> admin -> homepage (index)
	'_admin_homepage_box_1'              => 'معلومات النظام',
	'_admin_homepage_sysname'            => 'اسم النظام',
	'_admin_homepage_version'            => 'إصدار النظام',
	'_admin_homepage_programmer'         => 'مبرمج النظام',
	'_admin_homepage_website'            => 'موقع الدعم',
	'_admin_homepage_partner'            => 'الشركاء',
	'_admin_homepage_admin_des'          => 'تصميم لوحة التحكم',
	
	// right bar
	'_admin_right_welcome'               => 'مرحبا بك يا',
	'_admin_right_Homepage'              => 'رئيسية لوحة التحكم',
	'_admin_right_logout'                => 'تسجيل الدخول',
	'_admin_right_setting'               => 'الإعدادات',
		// sub-links (setting)
		'_admin_right_main_setting'   => 'الإعدادات العامة',
		'_admin_right_setting_read'   => 'إعدادات القراءة',
		'_admin_right_setting_write'  => 'إعدادات الكتابة',
		'_admin_right_setting_update' => 'التحديثات',
	
	'_admin_right_posts'                 => 'التدوينات',
		// sub-links (posts)
		'_admin_right_all_posts' => 'كل التدوينات',
		'_admin_right_new_post'  => 'تدوينة جديدة',
		'_admin_right_all_cats'  => 'الأقسام',
	'_admin_right_pages'                 => 'الصفحات',
		// sub-links (posts)
		'_admin_right_all_pages' => 'كل الصفحات',
		'_admin_right_new_page'  => 'صفحة جديدة',
	'_admin_right_comments'              => 'التعليقات',
	'_admin_right_styles'                => 'القوالب',
	'_admin_right_plugins'               => 'الاضافات البرمجية',
	'_admin_right_users'                 => 'الأعضاء',
	'_admin_right_utils'                 => 'الأدوات المساعدة',
	
	// stat box -> admin -> homepage (index)
	'_admin_homepage_stat_title'         => 'إحصائيات عامة',
	'_admin_homepage_stat_totalposts'    => 'المقالات',
	'_admin_homepage_stat_totalpages'    => 'الصفحات',
	'_admin_homepage_stat_totalcomments' => 'التعليقات',
	'_admin_homepage_stat_waitcomments'  => 'تعليقات غير مفعلة',
	'_admin_homepage_stat_totalusers'    => 'الأعضاء',
	'_admin_homepage_stat_media'         => 'الملفات',

	'_admin_language_submit' => 'حفظ',

	// Settings
		// main setting
		'site_url'               => 'رابط الموقع',
		'site_title'             => 'اسم الموقع',
		'site_description'       => 'وصف الموقع',
		'_admin_update_success' => 'تم التحديث بنجاح تام',

		// update
		'_admin_sys_info' => 'معلومات نظامك',
		'_admin_new_update' => 'تحديثات جديدة',

	// Posts
		// add post
		'_addpost_title'        => 'عنوان المقالة',
		'_addpost_title_note'   => '',
		'_addpost_urlname_page' => 'رابط الصفحة',
		'_addpost_urlname_post' => 'رابط التدوينة',
		'_addpost_status'       => 'إعدادات النشر',
		'_addpost_content'      => 'المحتوى',
		'_addpost_publish'      => 'منشور',
		'_addpost_draft'        => 'مسودة',
		'_addpost_wait'         => 'في الإنتضار',
		'_addpost_addnew'       => 'نشر',
		'_addpost_cat'          => 'الفسم',
		'_addpost_title'		=> 'تدوينة جديدة',
		'_addpage_title'		=> 'صفحة جديدة',
		'_addpost_thumbnail'    => 'الصورة المصغرة',

		// addpost/addpage error
		'_addpost_error_title'    => 'لم تقم بإدخال العنوان',
		'_addpost_error_name'     => 'لم تقم بإدخال الرابط',
		'_addpost_error_content'  => 'لم تقم بإدخال المحتوى',

	);