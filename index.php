<?php
/**
 * Part of the APBlog Core.
 *
 * @package    APBlog
 * @version    2.0
 * @author     APBlog Development Team
 * @license    GNU/GPL License
 * @copyright  2012 APBlog Development Team
 * @link       http://apblog.arabstep.com (parnter Arabstep.com)
 */

define('DEV_STAT', 'development');
define('DS', DIRECTORY_SEPARATOR);
define('CORE', dirname(__FILE__).DS.'core');
define('APP', dirname(__FILE__).DS.'apps');
define('THEMES_PATH',APP.DS.'themes');
define('PLUGINS_PATH',APP.DS.'plugins');


switch(DEV_STAT)
{
	case "development":
		error_reporting(E_ALL);
		break;
	case "testing":
	case "production":
		error_reporting(0);
		break;
	default:
		error_reporting(0);
		break;
}

include_once CORE.DS.'Common.php';