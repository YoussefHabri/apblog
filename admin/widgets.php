<?php include 'header.php';

// Select all the todos, ordered by position:
$_->db->query("SELECT * FROM `widgets` ORDER BY `position` ASC");

$todos = array();

// Filling the $todos array with new ToDo objects:

while($row = $_->db->fetchRow()){
	$todos[] = $_->loadClass('Widgets','',$row);
}

?>


	<ul class="todoList">
		
        <?php
		
		// Looping and outputting the $todos array. The __toString() method
		// is used internally to convert the objects to strings:
		
		foreach($todos as $item){
			echo $item;
		}
		
		?>

    </ul>

<a id="addButton" class="green-button" href="#">Add a ToDo</a>

</div>

<!-- This div is used as the base for the confirmation jQuery UI POPUP. Hidden by CSS. -->
<div id="dialog-confirm" title="Delete TODO Item?">Are you sure you want to delete this TODO item?</div>


<p class="note">The todos are flushed every hour. You can add only one in 5 seconds.</p>

<!-- Including our scripts -->

<script type="text/javascript" src="ui/js/jquery.min.js"></script>
<script type="text/javascript" src="ui/js/jquery-ui.min.js"></script>
<script type="text/javascript" src="js/script.js"></script>

</body>
</html>
<?php

$id = (int)$_GET['id'];
if(isset($_GET['action']) && !empty($_GET['action']))
	try{

		switch($_GET['action'])
		{
			case 'delete':
				$_->widgets->delete($id);
				break;
				
			case 'rearrange':
				$_->widgets->rearrange($_GET['positions']);
				break;
				
			case 'edit':
				$_->widgets->edit($id,$_GET['text']);
				break;
				
			case 'new':
				$_->widgets->createNew($_GET['text']);
				break;
		}

	}
	catch(Exception $e){
	//	echo $e->getMessage();
		die("0");
	}