<?php
include 'config_admin.php';

if (!$_->users->signed) {
    header("location:login.php");
} else {
	$stat =  array(
		'totalposts'    => $_->info->total('posts'),
		'totalpages'    => $_->info->total('pages'),
		'totalusers'    => $_->info->total('users'),
		'totalcomments' => $_->info->total('comments'),
		'waitcomments'  => $_->info->total('wait_comments'),
		'totalmedia'    => $_->info->total('media')
		);

	$assign = array(
		'title' => 'Admin Home',
		'upgrade' => '0',
		'stat' => $stat,
		'sec' => 'index'
		);

	$tpl->view('index',$assign);
}