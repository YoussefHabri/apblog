<?php include "config_admin.php";

if (!$_->users->signed) {
    header("location:login.php");
} else {

	$do = isset($_GET['do']) ? $_GET['do'] : null;
	$type = isset($_GET['type']) ? $_GET['type'] : 'post';
	$posts = &$_->posts;
	if($type == 'post'){ $tpl->assign('sec','posts'); }elseif($type == 'page'){ $tpl->assign('sec','pages'); }

	/*
	$hooks = $_->hooks;
	*/
	if($do == 'new'){
		$cat_sql = $_->db->query("SELECT * FROM posts WHERE post_type = 'cat'");
		$cat_res = $_->db->fetchRow();

		$type_httpget = isset($_GET['type']) ? 'type='.$type.'&' : '';

		$post = array(
			'submit'  => $lang['_addpost_addnew'],
			'urlname' => $lang['_addpost_urlname_'.$type],
			'title'   => '',
			'content' => '',
			'name'    => '',
			'type'    => $type,
			'cat_res' => $cat_res,
			'error'   => '',
			'type_httpget' => $type_httpget
			);

		$assign = array(
			'success' => null,
			'error'   => null,
			'p'       => $post,
			'title'   => $lang['_add'.$type.'_title']
			);

		if(isset($_POST['new_post']) && $_POST['new_post'] == 'done')
		{	
			$title   = xss_safe($_POST['post_title']);
			$name    = xss_safe($_POST['post_name']);
			$content = xss_safe($_POST['post_content']);
			$stat    = xss_safe($_POST['post_stat']);
			$cat_id  = valid_int($_POST['post_parent']);
			$author_id = $_->users->data['user_id'];
			if(empty($title)){
				$assign['success'] = '0';
				$assign['p']['error'] = $lang['_addpost_error_title'];
			}elseif(empty($name)){
				$assign['success'] = '0';
				$assign['p']['error'] = $lang['_addpost_error_name'];
			}elseif(empty($content)){
				$assign['success'] = '0';
				$assign['p']['error'] = $lang['_addpost_error_content'];
			}
			
			if(!file_exists(APP.DS.'data'.DS.'thumbnail'.DS.$_FILES['post_thumbnail']['name'])){
				if(copy($_FILES['post_thumbnail']['tmp_name'],APP.DS.'data'.DS.'thumbnail'.DS.$_FILES['post_thumbnail']['name']))
				{
					$pic = $_FILES['post_thumbnail']['name'];
				}
			}else{
				$pic = $_FILES['post_thumbnail']['name'];
			}
			
			$data = array(
				'post_title'     => $title,
				'post_name'      => $name,
				'post_content'   => $content,
				'post_parent'    => $cat_id,
				'post_author_id' => $author_id,
				'post_date'      => time(),
				'post_type'      => $type,
				'post_thumbnail' => $pic,
				'post_stat'      => $stat,
				);

			$_->posts->insert($data);

		}

		$tpl->view('addpost',$assign);
	}

}