<?php
include 'config_admin.php';

if ($_->users->signed) {
    header("location:index.php");
} else {
	$assign = array(
		'title' => 'Login',
		'loginError' => '',
		);

	if(isset($_POST) && !empty($_POST)) {
		$username = (isset($_POST['username']) ? $_POST['username'] : null);
        $password = (isset($_POST['password']) ? $_POST['password'] : null);
        $auto     = (isset($_POST['auto'])     ? $_POST['auto']     : null);

        print_r($_->users->signed);

        $_->users->login($username,$password,$auto);
        if($_->users->has_error()){
            $assign = array(
				'title' => 'Login',
				'loginError' => '1',
				);
        }else{
            header("location:index.php");
        }
	}

	$tpl->view('login',$assign);
}