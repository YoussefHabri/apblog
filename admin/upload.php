<?php
	// The Demos don't save files

	if (isset($_FILES["post_thumbnail"]) && is_uploaded_file($_FILES["post_thumbnail"]["tmp_name"]) && $_FILES["resume_file"]["error"] == 0) {
		echo rand(1000000, 9999999);	// Create a pretend file id, this might have come from a database.
		$_SESSION['filedata'] = $_FILES['post_thumbnail'];
	}
	
	exit(0);	// If there was an error we don't return anything and the webpage will have to deal with it.
?>