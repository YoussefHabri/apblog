<?php
/**
 * Part of the Omega Core.
 *
 * @package    Omega
 * @version    2.0
 * @author     Omega Development Team
 * @license    GNU/GPL License
 * @copyright  2012 Omega Development Team
 * @link       http://www.omega.om.cm
 */

define('DEV_STAT', 'development');
switch(DEV_STAT)
{
	case "development":
		error_reporting(E_ALL);
		break;
	case "testing":
	case "production":
		error_reporting(0);
		break;
	default:
		error_reporting(0);
		break;
}

define('DS', DIRECTORY_SEPARATOR);
define('CORE', dirname(dirname(__FILE__)).DS.'core');
define('APP', dirname(dirname(__FILE__)).DS.'apps');
define('ADMIN', dirname(__FILE__));
define('THEMES_PATH',APP.DS.'themes');
define('PLUGINS_PATH',APP.DS.'plugins');

define('IN_ADMIN', true);

include_once CORE.DS.'Common.php';

function blog_url() {
	$db_url = bloginfo('site_url');
	$url = (!empty($db_url) ? $db_url : dirname(dirname(siteurl())));
	return $url;
}

$tpl = $_->loadClass('RainTpl','','tpl');
$template_dir = ADMIN.DS.'theme'.DS;
$compile_dir  = ADMIN.DS.'theme'.DS.'cache'.DS;

$config = array( 
				"base_url"	=> null, 
				"tpl_dir"	=> $template_dir,
				"cache_dir"	=> $compile_dir,
				"tpl_ext"   => 'html',
				"debug"		=> true,
			   );
//use Rain;
RainTpl::configure( $config );


if(file_exists(APP.DS.'locale'.DS.DEFAULT_LANG.DS.'language_admin.php')){
	$lang = include APP.DS.'locale'.DS.DEFAULT_LANG.DS.'language_admin.php';
}

$tpl->assign('lang',$lang);
$tpl->assign('Url',blog_url().'/');
$tpl->assign('uinfo',$_->users->data);
$tpl->assign('sys_info', $_->version->sys_info());

?>