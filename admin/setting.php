<?php
include 'config_admin.php';

if (!$_->users->signed) {
    header("location:login.php");
} else {

	if(!isset($_GET['do']) or empty($_GET['do'])) {

		$assign = array(
			'title' => $lang['_admin_right_main_setting'],
			'sec' => 'setting',
			'success' => ''
			);

		if(isset($_POST['update']) && $_POST['update'] == 'done')
		{
			unset($_POST['update']);
			$_->info->update($_POST);
			if($_->info->has_error()){
				$assign['success'] = 0;
			}else{
				$assign['success'] = 1;
			}
		}

		$tpl->view('setting',$assign);

	}elseif(isset($_GET['do']) && $_GET['do'] == 'read'){
		$assign = array(
			'title' => $lang['_admin_right_setting_read'],
			'sec' => 'setting',
			'success' => ''
			);

		if(isset($_POST['update']) && $_POST['update'] == 'done')
		{
			unset($_POST['update']);
			$_->info->update($_POST);
			if($_->info->has_error()){
				$assign['success'] = 0;
			}else{
				$assign['success'] = 1;
			}
		}

		$tpl->view('read_setting',$assign);

	}elseif(isset($_GET['do']) && $_GET['do'] == 'update'){
		$sys_info = array(
			'engine' => array(
				'codename' => $_->Ecodename,
				'version' => $_->Eversion.$_->Edev_stat,
				),
			'codename' => $_->version->sysCodeName,
			'version' => $_->version->sysVer.'#'.$_->version->sysStat,
			);

		$assign = array(
			'title' => $lang['_admin_right_setting_update'],
			'sec' => 'setting',
			'sys_info' => $sys_info,
			'new_update' => array('true'=>'0','url'=>'','text'=>'')
			);

		$tpl->view('update',$assign);

	}

}