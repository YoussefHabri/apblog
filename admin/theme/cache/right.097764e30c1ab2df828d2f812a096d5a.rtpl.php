<?php if(!class_exists('RainTpl')){exit;}?>			<div id="profile-links">
				<?php echo $uinfo["username"]; ?> <?php echo $lang["_admin_right_welcome"]; ?>
				<br />

				<br />
				<a href="index.php" title="<?php echo $lang["_admin_right_Homepage"]; ?>"><?php echo $lang["_admin_right_Homepage"]; ?></a> | <a href="index.php?do=logout" title="<?php echo $lang["_admin_right_logout"]; ?>">
				<?php echo $lang["_admin_right_logout"]; ?></a>
			</div>

	<ul id="main-nav">  <!-- Accordion Menu -->
				<li>
					<a href="#" class="nav-top-item <?php if( $sec == 'setting' ){ ?> current <?php } ?>"> <!-- Add the class "current" to current menu item -->
						<?php echo $lang["_admin_right_setting"]; ?>
					</a>
					<ul>
						<!--<li><a href="setting.php?action=add"><?php echo $lang["_admin_right_setting_addsetting"]; ?></a></li>-->
						<li><a href="setting.php"><?php echo $lang["_admin_right_main_setting"]; ?></a></li>
						<li><a href="setting.php?do=read"><?php echo $lang["_admin_right_setting_read"]; ?></a></li>
						<!--<li><a href="setting.php?do=write"><?php echo $lang["_admin_right_setting_write"]; ?></a></li>-->
						<li><a href="setting.php?do=update"><?php echo $lang["_admin_right_setting_update"]; ?></a></li>
					</ul>
				</li>
	</ul>
	
		

	<ul id="main-nav">  <!-- Accordion Menu -->
				<li>
					<a href="#" class="nav-top-item <?php if( $sec == 'posts' ){ ?> current <?php } ?>"> <!-- Add the class "current" to current menu item -->
						<?php echo $lang["_admin_right_posts"]; ?>
					</a>
					<ul>
						<li><a href="posts.php"><?php echo $lang["_admin_right_all_posts"]; ?></a></li>
						<li><a href="posts.php?do=new"><?php echo $lang["_admin_right_new_post"]; ?></a></li>
						<li><a href="posts.php?type=category"><?php echo $lang["_admin_right_all_cats"]; ?></a></li> <!-- Add class "current" to sub menu items also -->
					</ul>
				</li>
	</ul>



	<ul id="main-nav">  <!-- Accordion Menu -->
				<li>
					<a href="#" class="nav-top-item <?php if( $sec == 'pages' ){ ?> current <?php } ?>"> <!-- Add the class "current" to current menu item -->
						<?php echo $lang["_admin_right_pages"]; ?>
					</a>
					<ul>
						<li><a href="posts.php?type=page"><?php echo $lang["_admin_right_all_pages"]; ?></a></li>
						<li><a href="posts.php?type=page&do=new"><?php echo $lang["_admin_right_new_page"]; ?></a></li>
					</ul>
				</li>
	</ul>
	
	

	<ul id="main-nav">  <!-- Accordion Menu -->
				<li>
					<a href="#" class="nav-top-item <?php if( $sec == 'comments' ){ ?> current <?php } ?>"> <!-- Add the class "current" to current menu item -->
						<?php echo $lang["_admin_right_comments"]; ?>
					</a>
					<ul>
						<li><a href="group.php?do=addnew"><?php echo $lang["_admin_group_addnew"]; ?></a></li>
						<li><a href="group.php?do=list"><?php echo $lang["_admin_group_list"]; ?></a></li>
					</ul>
				</li>
	</ul>

	<ul id="main-nav">  <!-- Accordion Menu -->
				<li>
					<a href="#" class="nav-top-item <?php if( $sec == 'styles' ){ ?> current <?php } ?>"> <!-- Add the class "current" to current menu item -->
						<?php echo $lang["_admin_right_styles"]; ?>
					</a>
					<ul>
						<li><a href="users.php?do=addnew"><?php echo $lang["_admin_users_addnew"]; ?></a></li>
						<li><a href="users.php?do=waiting"><?php echo $lang["_admin_users_waitinglist"]; ?></a></li>
						<li><a href="users.php?do=list"><?php echo $lang["_admin_users_list"]; ?></a></li>
					</ul>
				</li>
	</ul>


	<ul id="main-nav">  <!-- Accordion Menu -->
				<li>
					<a href="#" class="nav-top-item <?php if( $sec == 'plugins' ){ ?> current <?php } ?>"> <!-- Add the class "current" to current menu item -->
						<?php echo $lang["_admin_right_plugins"]; ?>
					</a>
					<ul>
						<li><a href="ext.php?do=addnew"><?php echo $lang["_admin_ext_addnew"]; ?></a></li>
						<li><a href="ext.php?do=list"><?php echo $lang["_admin_ext_list"]; ?></a></li>
					</ul>
				</li>
	</ul>


	<ul id="main-nav">  <!-- Accordion Menu -->
				<li>
					<a href="#" class="nav-top-item <?php if( $sec == 'users' ){ ?> current <?php } ?>"> <!-- Add the class "current" to current menu item -->
						<?php echo $lang["_admin_right_users"]; ?>
					</a>
					<ul>
						<li><a href="pages.php?do=addnew"><?php echo $lang["_admin_pages_addnew"]; ?></a></li>
						<li><a href="pages.php?do=list"><?php echo $lang["_admin_pages_list"]; ?></a></li>
					</ul>
				</li>
	</ul>


	<ul id="main-nav">  <!-- Accordion Menu -->
				<li>
					<a href="#" class="nav-top-item <?php if( $sec == 'utils' ){ ?> current <?php } ?>"> <!-- Add the class "current" to current menu item -->
						<?php echo $lang["_admin_right_utils"]; ?>
					</a>
					<ul>
						<li><a href="comments.php?do=wait"><?php echo $lang["_admin_comments_wiat"]; ?></a></li>
						<li><a href="comments.php?do=list"><?php echo $lang["_admin_comments_list"]; ?></a></li>
					</ul>
				</li>
	</ul>