<?php if(!class_exists('RainTpl')){exit;}?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" dir="rtl">

	<head>

		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />

		<title>TRAIDNT UPLOAD - <?php echo $title; ?></title>

		<!--                       CSS                       -->

		<!-- Reset Stylesheet -->
		<link rel="stylesheet" href="<?php echo $Url; ?>admin/theme/css/reset.css" type="text/css" media="screen" />

		<!-- Main Stylesheet -->

		<link rel="stylesheet" href="<?php echo $Url; ?>admin/theme/css/style.css" type="text/css" media="screen" />

		<!-- Invalid Stylesheet. This makes stuff look pretty. Remove it if you want the CSS completely valid -->
		<link rel="stylesheet" href="<?php echo $Url; ?>admin/theme/css/invalid.css" type="text/css" media="screen" />

		<link rel="stylesheet" href="<?php echo $Url; ?>admin/theme/css/blue.css" type="text/css" media="screen" />


		<!-- Internet Explorer Fixes Stylesheet -->

		<!--[if lte IE 7]>
			<link rel="stylesheet" href="<?php echo $Url; ?>admin/theme/css/ie.css" type="text/css" media="screen" />
		<![endif]-->

		<!--                       Javascripts                       -->

		<!-- jQuery -->
		<script type="text/javascript" src="<?php echo $Url; ?>admin/theme/js/jquery-1.3.2.min.js"></script>


		<!-- jQuery Configuration -->
		<script type="text/javascript" src="<?php echo $Url; ?>admin/theme/js/simpla.jquery.configuration.js"></script>

		<!-- Facebox jQuery Plugin -->
		<script type="text/javascript" src="<?php echo $Url; ?>admin/theme/js/facebox.js"></script>

		<!-- jQuery WYSIWYG Plugin -->
		<script type="text/javascript" src="<?php echo $Url; ?>admin/theme/js/jquery.wysiwyg.js"></script>

		<!-- jQuery Datepicker Plugin -->

		<script type="text/javascript" src="<?php echo $Url; ?>admin/theme/js/jquery.datePicker.js"></script>
		<script type="text/javascript" src="<?php echo $Url; ?>admin/theme/js/jquery.date.js"></script>
		<!--[if IE]><script type="text/javascript" src="<?php echo $Url; ?>admin/theme/js/jquery.bgiframe.js"></script><![endif]-->


		<!-- Internet Explorer .png-fix -->

		<!--[if IE 6]>
			<script type="text/javascript" src="<?php echo $Url; ?>admin/theme/js/DD_belatedPNG_0.0.7a.js"></script>
			<script type="text/javascript">
				DD_belatedPNG.fix('.png_bg, img, li');
			</script>
		<![endif]-->

	</head>

	<body><div id="body-wrapper"> <!-- Wrapper for the radial gradient background -->


		<div id="sidebar">
		<div id="sidebar-wrapper"> <!-- Sidebar with logo and menu -->

			<h1 id="sidebar-title"><a href="#">TRAIDNT UPLOAD</a></h1>

			<!-- Logo (221px wide) -->
			<a href="#"><img id="logo" src="<?php echo $Url; ?>admin/theme/images/logo.png" alt="TRAIDNT UPLOAD" /></a>

			<!-- Sidebar Profile links -->

			<?php $tpl = new RainTpl;$tpl_dir_temp = static::$conf['tpl_dir'];$tpl->assign( $this->var );$tpl->draw( dirname("right") . ( substr("right",-1,1) != "/" ? "/" : "" ) . basename("right") );?>

			 <!-- End #main-nav -->


		</div></div> <!-- End #sidebar -->

		<div id="main-content"> <!-- Main Content Section with everything -->

			<!-- Page Head -->

			<div class="clear"></div> <!-- End .clear -->

			<div class="content-box"><!-- Start Content Box -->

				<div class="content-box-header">

					<h3><?php echo $title; ?></h3>

					<div class="clear"></div>

				</div> <!-- End .content-box-header -->

				<div class="content-box-content">

					<div class="tab-content default-tab" >
					<!-- This is the target div. id must match the href of this div's tab -->
						<?php if( $success == '0' ){ ?>
						<div class="notification error  png_bg">
							<div>
								<?php echo $p["error"]; ?>
							</div>
						</div>
						<?php }elseif( $success == '1' ){ ?>
						<div class="notification success  png_bg">
							<div>
								<?php echo $lang["_admin_action_success"]; ?>
							</div>
						</div>

						<?php } ?>
					<form action="posts.php?<?php echo $p["type_httpget"]; ?>do=new" method="post" enctype="multipart/form-data">
						<input type="hidden" name="new_post" value="done">
						<table style="width: 100%; border:thin silver solid;  " cellpadding="2">
							<tbody>
								<tr>
									<td><?php echo $lang["_addpost_title"]; ?>:</td>
									<td style="text-align:right;padding-right:10px;">
										<input class="text-input small-input" value="<?php echo $p["title"]; ?>" dir="ltr"  name="post_title" type="text" /> &nbsp;&nbsp; <?php echo $lang["_addpost_title_note"]; ?>
										<span class="input-notification png_bg"><?php echo $error["title"]; ?></span>

									</td>
								</tr>

								<tr>
									<td><?php echo $p["urlname"]; ?>:</td>
									<td style="text-align:right;padding-right:10px;">
										<input class="text-input small-input" value="<?php echo $p["name"]; ?>"  name="post_name" type="text" />
										<span class="input-notification png_bg"></span>

									</td>
								</tr>

								<tr>
									<td><?php echo $lang["_addpost_status"]; ?> :</td>
									<td style="text-align:right;padding-right:10px;">

											<select size="1" name="post_stat">
												<option value="publish"><?php echo $lang["_addpost_publish"]; ?></option>
												<option value="draft"><?php echo $lang["_addpost_draft"]; ?></option>
												<option value="wait"><?php echo $lang["_addpost_wait"]; ?></option>
											</select>
										<span class="input-notification png_bg"></span>


									</td>
								</tr>
								<?php if( $p["type"] != 'page' ){ ?>
								<tr>
									<td><?php echo $lang["_addpost_cat"]; ?> :</td>
									<td style="text-align:right;padding-right:10px;">

											<select size="1" name="post_parent">
											<?php $counter1=-1; if( is_array($cat_res) && sizeof($cat_res) ) foreach( $cat_res as $key1 => $value1 ){ $counter1++; ?>
												<option value="{value.post_id}"><?php echo $value1["post_title"]; ?></option>
											<?php } ?>
											</select>
										<span class="input-notification png_bg"></span>


									</td>
								</tr>
								<?php } ?>
								<tr>
									<td><?php echo $lang["_addpost_content"]; ?> :</td>
									<td style="text-align:right;padding-right:10px;">

										<textarea name="post_content" dir="ltr" style="height:150px;"><?php echo $p["content"]; ?></textarea>

										<span class="input-notification png_bg"></span>
									</td>
								</tr>

								<tr>
									<td><?php echo $lang["_addpost_thumbnail"]; ?> :</td>
									<td style="text-align:right;padding-right:10px;">

										<input type="file" name="post_thumbnail">

										<span class="input-notification png_bg"></span>
									</td>
								</tr>

								<tr>
									<td style="width: 200px;height:40px; border-left:thin silver solid; text-align:center; vertical-align:middle;"></td>
									<td style="text-align:right; padding-right:10px;  vertical-align:middle;">
										<input class="button" value="<?php echo $p["submit"]; ?>" type="submit" />
									</td>
								</tr>

							</tbody>
						</table>
					</form>

					</div> <!-- End #tab1 -->



				</div> <!-- End .content-box-content -->

			</div> <!-- End .content-box -->


			<div id="footer">
				<small> <!-- Remove this notice or replace it with whatever you want -->

						<?php echo $lang["_admin_copyright"]; ?> | <a href="#top">Top</a>
				</small>
			</div><!-- End #footer -->

		</div> <!-- End #main-content -->

	</div></body>


</html>
