<?php if(!class_exists('RainTpl')){exit;}?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" dir="rtl">

	<head>

		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />

		<title><?php echo $sys_info["name"]; ?> | <?php echo $lang["_admin_login"]; ?></title>

		<!--                       CSS                       -->

		<!-- Reset Stylesheet -->
		<link rel="stylesheet" href="<?php echo $Url; ?>admin/theme/css/reset.css" type="text/css" media="screen" />

		<!-- Main Stylesheet -->

		<link rel="stylesheet" href="<?php echo $Url; ?>admin/theme/css/style.css" type="text/css" media="screen" />

		<!-- Invalid Stylesheet. This makes stuff look pretty. Remove it if you want the CSS completely valid -->
		<link rel="stylesheet" href="<?php echo $Url; ?>admin/theme/css/invalid.css" type="text/css" media="screen" />


		<!--[if lte IE 7]>
			<link rel="stylesheet" href="<?php echo $Url; ?>admin/theme/css/ie.css" type="text/css" media="screen" />
		<![endif]-->

		<!--                       Javascripts                       -->

		<!-- jQuery -->
		<script type="text/javascript" src="<?php echo $Url; ?>admin/theme/js/jquery-1.3.2.min.js"></script>


		<!-- jQuery Configuration -->
		<script type="text/javascript" src="<?php echo $Url; ?>admin/theme/js/simpla.jquery.configuration.js"></script>

		<!-- Facebox jQuery Plugin -->
		<script type="text/javascript" src="<?php echo $Url; ?>admin/theme/js/facebox.js"></script>

		<!-- jQuery WYSIWYG Plugin -->
		<script type="text/javascript" src="<?php echo $Url; ?>admin/theme/js/jquery.wysiwyg.js"></script>

		<!-- Internet Explorer .png-fix -->


		<!--[if IE 6]>
			<script type="text/javascript" src="<?php echo $Url; ?>admin/theme/js/DD_belatedPNG_0.0.7a.js"></script>
			<script type="text/javascript">
				DD_belatedPNG.fix('.png_bg, img, li');
			</script>
		<![endif]-->

	</head>

	<body id="login">

		<div id="login-wrapper" class="png_bg">
			<div id="login-top">
				<!-- Logo (221px width) -->
				<img id="logo" src="<?php echo $Url; ?>admin/theme/images/logo.png" />

			</div> <!-- End #logn-top -->

			<div id="login-content">

				<form action="login.php" method="post">
					<?php if( $loginError == '1' ){ ?>
					<div class="notification attention png_bg">
						<div>
							<?php echo $lang["_admin_login_error"]; ?>
						</div>
					</div>
					<?php } ?>
					<p>

						<label><?php echo $lang["_admin_login_username"]; ?></label>
						<input class="text-input" type="text" name="username" />
					</p>
					<div class="clear"></div>
					<p>
						<label><?php echo $lang["_admin_login_password"]; ?></label>
						<input class="text-input" type="password" name="password" />
					</p>

					<div class="clear"></div>
					<p id="remember-password">
						<input type="checkbox" name="auto" value="true" id="auto"> تدكرني؟
					</p>
					<div class="clear"></div>
					<p>
						<input class="button" type="submit" value="<?php echo $lang["_admin_login_submit"]; ?>" />
					</p>

				</form>

			</div> <!-- End #login-content -->

		</div> <!-- End #login-wrapper -->

  </body>

</html>
